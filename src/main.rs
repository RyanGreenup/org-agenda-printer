use chrono::NaiveTime;
use chrono::{self, Datelike, NaiveDate, Timelike};
use clap::Parser;
use std::cmp::Ordering;
use std::fs;
use std::process::exit;

// TODO repeating tasks are broken
// SCHEDULED: <2022-04-07 Thu 15:15 ++4d>
// TODO zhis doesn't seem to work
// SCHEDULED: <2022-04-07 11:00>
// TODO NEXT tags should be filtered out aswell
// debug with this
// print!("issue here on:\t{}\n", &h.title(&org).raw as &str);

/// Print scheduled items from an org agenda
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Visualise Task Duration
    #[clap(short, long)]
    space_duration: bool,

    /// Dump JSON and Exit
    #[clap(short, long)]
    dump_json: bool,

    /// Convert to Remind format and Exit
    #[clap(long)]
    rem: bool,

    // Print Overdue Agenda Items
    #[clap(short, long)]
    overdue: bool,
    // Print Agenda Items for Today
    #[clap(short, long)]
    today: bool,
    // Print upcoming Agenda Items
    #[clap(short, long)]
    upcoming: bool,

    /// Path to org-mode files that will parsed
    files: Vec<String>,
}

use orgize::{
    elements::{Datetime, Timestamp},
    Org,
};
use serde_json::to_string;
struct Task {
    title: String,
    date: chrono::NaiveDate,
    time: Option<NaiveTime>,
    duration: Option<i64>, // Duration in hours, // TODO should this be end date?
}

fn print_tasks_as_table(tasks: &Vec<&Task>, print_range: (bool, bool, bool)) {
    // This should print out a table of tasks corresponding to the same date
    let date_of_all_tasks = tasks[0].date;

    match chrono::Local::now()
        .num_days_from_ce()
        .cmp(&date_of_all_tasks.num_days_from_ce())
    {
        Ordering::Less => {
            if !print_range.2 {
                return;
            }
        } // today is less than these tasks, meaning there upcoming,
        Ordering::Equal => {
            if !print_range.1 {
                return;
            }
        }
        Ordering::Greater => {
            if !print_range.0 {
                return;
            }
        }
    }

    let date_of_all_tasks = date_of_all_tasks.format("%A, %-d %B, %C%y").to_string();

    println!("\n** {}\n", date_of_all_tasks);
    println!("|Time | len | Task                                                                       {:<0}|", "");
    println!("|-----+-----+ ---------------------------------------------------------------------------{:<0}|", "");
    for t in tasks {
        let d = match t.duration {
            Some(d) => d.to_string(),
            None => String::from(""),
        };
        let time = match t.time {
            Some(tm) => tm.format("%H:%M").to_string(),
            None => String::from(""),
        };
        println!("|{:<5}|  {:<3}|{:<76}|", time, d, t.title);
    }
}

fn main() {
    let args = Args::parse();

    let mut org_mode_text = String::new();
    for file in &args.files {
        let file_mode_text = match fs::read_to_string(file) {
            Ok(s) => s,
            Err(e) => {
                // TODO this should be STDERR
                eprintln!("No org file provided");
                eprintln!("{}", e);
                // TODO don't exit because I'd rather it just skip with a warning
                String::new()
                //                 exit(1)
            }
        };
        org_mode_text.push_str(&file_mode_text);
    }

    let org_mode_text = strip_footnotes(org_mode_text.to_string());
    let org = Org::parse(org_mode_text.as_str());

    let org_json = to_string(&org).unwrap();

    if args.dump_json {
        println!("{}", &org_json);
        exit(0);
    };


    let mut mytasks: Vec<Task> = Vec::new();

    for h in org.headlines() {
        let out = match h.title(&org).scheduled() {
            Some(t) => t,
            None => continue,
        };

        let scheduled_date = match out {
            Timestamp::Active {
                start,
                repeater: _,
                delay: _,
            } => (start, start), // TODO duration = 0 is problem??
            Timestamp::ActiveRange {
                start,
                end,
                repeater: _,
                delay: _,
            } => (start, end),
            _ => {
                eprintln!("Non Active Time stamp!");
                continue;
            } // likely inactive timeblock
        };

        let (start_scheduled_date, start_scheduled_time) = datetime_to_naive_date(scheduled_date.0);
        let (end_scheduled_date, end_scheduled_time) = datetime_to_naive_date(scheduled_date.1);
        let duration = calculate_duration(
            &start_scheduled_date,
            &start_scheduled_time,
            &end_scheduled_date,
            &end_scheduled_time,
        );
        let title = (&h.title(&org).raw as &str).to_string();

        mytasks.push(Task {
            title,
            date: start_scheduled_date,
            time: start_scheduled_time,
            duration,
        });
    }

    if args.rem {
        org2rem(mytasks);
        exit(0)
    }


    mytasks.sort_by(|a, b| a.date.cmp(&b.date));

    if !args.overdue && !args.today && !args.upcoming {
        // If no agenda range selected, print all agenda
        print_tasks(&mytasks, (true, true, true));
    } else {
        // Otherwise print the items selected
        print_tasks(&mytasks, (args.overdue, args.today, args.upcoming));
    }
}

fn org2rem(tasks: Vec<Task>) {

    println!("# This was converted from org mode on: {}", chrono::Local::now().format("%A, %-d %B, %C%y %H:%M").to_string());
    for t in tasks {
        let time = match t.time {
            Some(time) => t.date.and_time(time).format("%Y-%m-%d %H:%M:%S").to_string(),
            None => t.date.format("%Y-%m-%d").to_string(),
        };

        match t.duration {
            Some(d) => println!("REM {} DURATION {} MSG {}", time, d, t.title),
            None => println!("REM {} MSG {}", time, t.title),
        }
    }

}

// TODO instead have an optional end date and a function to calculate the duration
fn calculate_duration(
    start_scheduled_date: &NaiveDate,
    start_scheduled_time: &Option<NaiveTime>,
    end_scheduled_date: &NaiveDate,
    end_scheduled_time: &Option<NaiveTime>,
) -> Option<i64> {
    match end_scheduled_time {
        Some(et) => match start_scheduled_time {
            Some(st) => {
                let start = start_scheduled_date.and_hms(st.hour(), st.minute(), 0);
                let end = end_scheduled_date.and_hms(et.hour(), et.minute(), 0);
                let duration = start - end;
                let duration = duration.num_hours();
                Some(duration)
            }
            None => None,
        },
        None => None,
    }
}

fn datetime_to_naive_date(in_org_date: &Datetime) -> (NaiveDate, Option<NaiveTime>) {
    let out_naive_date = NaiveDate::from_ymd(
        in_org_date.year as i32,
        in_org_date.month as u32,
        in_org_date.day as u32,
    );

    let out_naive_time: Option<NaiveTime> = match in_org_date.hour {
        Some(h) => {
            let m = match in_org_date.minute {
                Some(m) => m,
                None => 0,
            };
            Some(NaiveTime::from_hms(h as u32, m as u32, 0))
        }
        None => None,
    };
    return (out_naive_date, out_naive_time);
}

// TODO space_duration should be a CLI argument
fn print_tasks(tasks_vec: &Vec<Task>, print_range: (bool, bool, bool)) {
    let mut t_old = if tasks_vec.len() >= 1 {
        &tasks_vec[1]
    } else {
        return;
    };

    let mut tasks_batch: Vec<&Task> = vec![];
    for t in tasks_vec {
        // If a day has passed from the ordered vector, print a line
        let distance = t.date.num_days_from_ce() - t_old.date.num_days_from_ce();
        if distance > 0 {
            print_tasks_as_table(&tasks_batch, print_range);
            tasks_batch = vec![];
        };

        tasks_batch.append(&mut vec![t]);

        t_old = t;
    }
}

fn strip_footnotes(in_text: String) -> String {
    // must remove all lines that contain footnotes due to a bug in orgize
    return in_text.replace("[fn:", "");
}
